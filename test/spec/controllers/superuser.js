'use strict';

describe('Controller: SuperuserCtrl', function () {

  // load the controller's module
  beforeEach(module('flowershopApp'));

  var SuperuserCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SuperuserCtrl = $controller('SuperuserCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(SuperuserCtrl.awesomeThings.length).toBe(3);
  });
});
