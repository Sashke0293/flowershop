'use strict';

/**
 * @ngdoc service
 * @name flowershopApp.connector
 * @description
 * # connector
 * Service in the flowershopApp.
 */
angular.module('flowershopApp')
  .service('connector', ['$http',function ($http) {

    var connector = {
      load: function (obj) {

        obj = obj || {};
        obj.path = obj.path || '';
        obj.url = obj.path || '';
        obj.data = obj.data || {};
        obj.method = obj.method || 'GET';

        obj.success = obj.success || function (data) {
            console.log(data);
          };
        obj.error = obj.error || function (data) {
            console.log(data);
          };

        $http({
          method: obj.method,
          url: obj.url,
          path: obj.path,
          data: obj.data
        }).then(function successCallback(response) {

          obj.success(response);

        }, function errorCallback(response) {

          obj.error(response);

        });

      }
    }

    return connector;

  }]);
