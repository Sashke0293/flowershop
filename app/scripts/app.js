'use strict';

/**
 * @ngdoc overview
 * @name flowershopApp
 * @description
 * # flowershopApp
 *
 * Main module of the application.
 */


// сумма за период (по товару и по списанному и по проданному)
// добавить actions(пополнение)
// сколько в кассе денег
// сколько на терминале денег


angular
  .module('flowershopApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'LocalStorageModule'
  ])
  .config(function ($routeProvider,localStorageServiceProvider) {
    localStorageServiceProvider
      .setStorageType('sessionStorage');
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/check/:id', {
        templateUrl: 'views/templates/check.html',
        controller: 'checkCtrl',
        controllerAs: 'check'
      })
      .when('/superuser', {
        templateUrl: 'views/superuser.html',
        controller: 'SuperuserCtrl',
        controllerAs: 'superuser'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .controller('AppCtrl', ['$scope', 'connector', 'localStorageService', '$location',function ($scope,connector, localStorageService, $location) {

    $scope.loggedIn = true;
    if(!/\/check.*/gi.test($location.path())){
      $scope.loggedIn = false;

      console.log(localStorageService.get('user'));
      $scope.user = localStorageService.get('user');
      if($scope.user != null){
        connector.load({
          path: 'http://arduino.tt/php/flower.php',
          method: 'POST',
          data: {
            action: 'login',
            data: {
              login: $scope.user.name,
              raw: $scope.user.password
            }
          },
          success: function (data) {
            if(data.data.status == "success"){
              $scope.loggedIn = true;
              $scope.user = {
                name: data.data.result1.login,
                password: data.data.result1.password,
                level: data.data.result1.priority,
                users_id: data.data.result1.users_id
              }
              localStorageService.set('user', $scope.user);
            }else{
              $scope.loggedIn = false;
            }
          }
        });
      }
    }

    $scope.login = function () {
      connector.load({
        path: 'http://arduino.tt/php/flower.php',
        method: 'POST',
        data: {
          action: 'login',
          data: {
            login: $scope.username,
            password: $scope.password
          }
        },
        success: function (data) {
          if(data.data.status == "success"){
            $scope.loggedIn = true;
            $scope.user = {
              name: data.data.result1.login,
              password: data.data.result1.password,
              level: data.data.result1.priority,
              users_id: data.data.result1.users_id
            }
            localStorageService.set('user', $scope.user);
          }else{
            $scope.loggedIn = false;
          }
        }
      });
    }

    $scope.logout = function () {
      $scope.loggedIn = false;
      localStorageService.set('user', null);
    }

  }]);
