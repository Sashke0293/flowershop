'use strict';

/**
 * @ngdoc function
 * @name flowershopApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the flowershopApp
 */
angular.module('flowershopApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
