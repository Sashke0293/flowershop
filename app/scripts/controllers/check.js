'use strict';

/**
 * @ngdoc function
 * @name flowershopApp.controller:checkCtrl
 * @description
 * # AboutCtrl
 * Controller of the flowershopApp
 */
angular.module('flowershopApp')
  .controller('checkCtrl', ['$scope', 'connector', '$routeParams',function ($scope,connector,$routeParams) {

    $scope.order = {};
    $scope.checknum = $routeParams.id;
    $scope.sum = 0;

    connector.load({
      path: 'http://arduino.tt/php/flower.php',
      method: 'POST',
      data: {
        action: 'getOrder',
        data: {
          id: $routeParams.id
        }
      },
      success: function (data) {
        $scope.orders = data.data.result1;
        for(var i in $scope.orders){
          $scope.sum += $scope.orders[i].price * $scope.orders[i].count;
        }
      }
    });

  }]);
