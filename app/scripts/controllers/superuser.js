'use strict';

/**
 * @ngdoc function
 * @name flowershopApp.controller:SuperuserCtrl
 * @description
 * # SuperuserCtrl
 * Controller of the flowershopApp
 */
angular.module('flowershopApp')
  .controller('SuperuserCtrl', ['$scope', 'connector', 'localStorageService',function ($scope,connector, localStorageService) {

    $scope.addCount = 0;

    $scope.manager = {
      addProduct: function () {
        connector.load({
          path: 'http://arduino.tt/php/flower.php',
          method: 'POST',
          data: {
            action: 'addProduct',
            data: {
              product: {
                name: $scope.item_name,
                price: $scope.item_price,
                products_types_products_types_id: $scope.item_type,
                img: $scope.item_url
              }
            }
          },
          success: function (data) {

          }
        });
      },
      updateProduct: function (createProduct) {
        connector.load({
          path: 'http://arduino.tt/php/flower.php',
          method: 'POST',
          data: {
            action: 'updateProduct',
            name: createProduct.name,
            price: createProduct.price,
            addcount: createProduct.count,
            type: createProduct.type
          },
          success: function (data) {

          }
        });
      }
    }

    $scope.selectProduct = function (product) {
      $scope.selectedProduct = product;
    }

    $scope.updateProduct = function (product) {
      $scope.user = localStorageService.get('user');

      if($scope.user.level == 1){
        var product = {
          'products_id': $scope.selectedProduct.products_id,
          'name': $scope.selectedProduct.name,
          'price': $scope.selectedProduct.price,
          'img': $scope.selectedProduct.img || "",
          'count': $scope.addCount
        }

        connector.load({
          path: 'http://arduino.tt/php/flower.php',
          method: 'POST',
          data: {
            action: 'change',
            data: {
              product: product,
              user: $scope.user
            }
          },
          success: function (data) {
            console.log(data);
            $scope.getProductsList();
          }
        });
      }else{
        alertify.error("У вас недостаточно привилегий для этой операции");
      }
    }

    $scope.addProductCount = function () {

      $scope.user = localStorageService.get('user');

      if($scope.user.level == 1){
        var products = [{
          'products_id': $scope.selectedProduct.products_id,
          'name': $scope.selectedProduct.name,
          'price': $scope.selectedProduct.price,
          'count': $scope.addCount
        }]

        connector.load({
          path: 'http://arduino.tt/php/flower.php',
          method: 'POST',
          data: {
            action: 'add',
            data: {
              products: products,
              paytype: 'другое',
              user: $scope.user
            }
          },
          success: function (data) {
            console.log(data);
            $scope.getProductsList();
          }
        });
      }else{
        alertify.error("У вас недостаточно привилегий для этой операции");
      }

    }


    $scope.getProductsList = function () {
      connector.load({
        path: 'http://arduino.tt/php/flower.php',
        method: 'POST',
        data: {
          action: 'getProductsList'
        },
        success: function (data) {
          $scope.productsList = data.data.result1;
        }
      });
    }
    $scope.getProductsList();

  }]);
