'use strict';

/**
 * @ngdoc function
 * @name flowershopApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the flowershopApp
 */
angular.module('flowershopApp')
  .controller('MainCtrl', ['$scope','$uibModal','connector','$http', 'localStorageService',function ($scope,$uibModal,connector,$http, localStorageService) {

    $scope.selectedProduct = {
      'name': 'Роза 60',
      'count': 0
    };

    $scope.user = localStorageService.get('user');

    $scope.payType = true;

    $scope.cart = {
      form: function () {

        console.log($scope.cart.inner);

          $http({
            method: 'POST',
            url: 'http://arduino.tt/php/flower.php',
            data: {
              action: 'sell',
              data: {
                products: $scope.cart.inner,
                paytype: $scope.payType,
                user: $scope.user
              }
            }
          }).then(function successCallback(response) {


            console.log(response);
            $scope.getProducts();

            var check = window.open("#/check/"+response.data.result1.id, "check","status=1,width="+window.innerWidth+",height="+window.innerHeight);
            $scope.cart.inner = [];
            $scope.$apply()

          }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
          });

        // window.onafterprint = function () {;
        // }
        //
        // setTimeout(window.onafterprint,3000);

      },
      addToCart: function (product, count) {

        for(var prods in $scope.cart.inner){
          if(product.id == $scope.cart.inner[prods].products_id){
            $scope.cart.inner[prods].count += count;
            return;
          }
        }

        $scope.cart.inner.push({
          'products_id': product.products_id,
          'name': product.name,
          'price': product.price,
          'count': count,
          'inc': function () {
            this.count = Number(this.count) + 1;
          },
          'dec': function () {
            this.count = Number(this.count) - 1;
          }
        });

      },
      removeFromCart: function (products_id) {

        $scope.cart.inner.splice(products_id,1);

      },
      inner: []
    };

    $scope.getProducts = function () {
      $http({
        method: 'POST',
        url: 'http://arduino.tt/php/flower.php',
        data: {
          action: 'getProducts'
        }
      }).then(function successCallback(response) {
        $scope.products = {
          main:[],
          decoration:[],
          other:[]
        }
        for(var i in response.data.table){

          console.log(response.data.table[i]);

          switch(Number(response.data.table[i].products_types_products_types_id)){
            case 1:
              $scope.products.main.push(response.data.table[i]);
              break;
            case 2:
              $scope.products.decoration.push(response.data.table[i]);
              break;
            case 3:
              $scope.products.other.push(response.data.table[i]);
              break;
          }
        }

      }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    }

    $scope.getProducts();


    $scope.getSumma = function () {
      var summ = 0;
      for(var prods in $scope.cart.inner){
        summ += Number($scope.cart.inner[prods].price) * $scope.cart.inner[prods].count;
      }

      console.log($scope.cart);

      return summ;

    };

    $scope.getPercent = function () {

      var sold = 0;
      var all = 0;

      for(var prods in $scope.products){
        for(var prod in $scope.products[prods]){
          sold += $scope.products[prods][prod].sold;
          all += $scope.products[prods][prod].sold + $scope.products[prods][prod].removed + $scope.products[prods][prod].count;
        }
      }

      return (sold/all)*100;

    };

    $scope.buy = function () {

    };

    $scope.getPercentRemoved = function () {

      var removed = 0;
      var all = 0;

      for(var prods in $scope.products){
        for(var prod in $scope.products[prods]){
          removed += $scope.products[prods][prod].removed;
          all += $scope.products[prods][prod].sold + $scope.products[prods][prod].removed + $scope.products[prods][prod].count;
        }
      }

      return (removed/all)*100;

    }

    $scope.open = function (size, product) {

      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'myModalContent.html',
        controller: 'ModalInstanceCtrl',
        size: size,
        resolve: {
          product: function () {
            return product;
          },
          products: function () {
            return $scope.products;
          },
          cart: function () {
            return $scope.cart;
          }
        }
      });

      modalInstance.result.then(function (obj) {
        $scope.cart = obj.cart;
        $scope.products = obj.products;
        // console.log(products);
        // $scope.selectedProduct = selectedItem;
      }, function () {
        console.info('Modal dismissed at: ' + new Date());
      });
    };

  }]);

angular.module('flowershopApp').controller('ModalInstanceCtrl', function ($scope, $http, $uibModalInstance, product, products, cart, localStorageService) {

  $scope.selectedProduct = product;

  $scope.itemcount = 0;

  $scope.user = localStorageService.get('user');

  $scope.paytype = 'money';

  $scope.inc = function () {
    $scope.itemcount++;
  };
  $scope.dec = function () {
    $scope.itemcount--;
  };

  $scope.$watch('itemcount', function () {
    if($scope.itemcount < 0){
      $scope.itemcount = 0;
    }
  });

  $scope.ok = function () {

    cart.addToCart(product, $scope.itemcount);

    $uibModalInstance.close({cart:cart,products:products});

  };

  $scope.removed = function () {

    $scope.user = localStorageService.get('user');
    if($scope.user != null){

      $http({
        method: 'POST',
        url: 'http://arduino.tt/php/flower.php',
        data: {
          action: 'remove',
          data: {
            products_id: product.products_id,
            count: $scope.itemcount,
            user: $scope.user
          }
        }
      }).then(function successCallback(response) {

        window.location = '/#';
        $uibModalInstance.dismiss('cancel');

      }, function errorCallback(response) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    }else{
      alertify.log("Вы не вошли в систему");
    }

  };

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
